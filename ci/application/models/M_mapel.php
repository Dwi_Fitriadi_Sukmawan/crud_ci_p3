<?php 

defined('BASEPATH') OR exit ('No direct script access allowed');

  /**
   * 
   */
  class M_mapel extends CI_Model
  {
    
    public function getData()
    {
      $this->db->select('*');
      $this->db->from('mapel');
      $data = $this->db->get();

      return $data->result();
    }

    public function getDetailMapel($id)
    {
      $this->db->where('id',$id);
      $data = $this->db->get('mapel')->result();
      return $data[0];
    }

    public function insertMapel($param)
    {
      $object = [
        'mapel' => $param['mapel']
      ];
      $this->db->insert('mapel',$object);

      return $this->db->affected_rows();
    }

    public function editMapel($param)
    {
      $object = [
        'mapel' => $param['mapel']
      ];
      $this->db->where('id', $param['id']);
      $this->db->update('mapel',$object);

      return $this->db->affected_rows();
    }

    public function delete($id)
    {
      $this->db->where('id',$id);
      $this->db->delete('mapel');

      return $this->db->affected_rows();
    }

  }

 ?>