<?php 

defined('BASEPATH') OR exit ('No direct script access allowed');

  /**
   * 
   */
  class M_siswa extends CI_Model
  {
    
    public function getData()
    {
      $this->db->select('*');
      $this->db->from('siswa');
      $data = $this->db->get();

      return $data->result();
    }

    public function getDetailSiswa($id)
    {
      $this->db->where('id',$id);
      $data = $this->db->get('siswa')->result();
      return $data[0];
    }

    public function insertSiswa($param)
    { 
      // unset($act);
      $object = [
        'nama' => $param['nama'],
        'umur' => $param['umur']
      ];
      $this->db->insert('siswa', $object);

      return $this->db->affected_rows();

      // print_r($param);
    }

    public function editSiswa($param)
    {
      $object = [
        'nama' => $param['nama'],
        'umur' => $param['umur']
      ];
      $this->db->where('id', $param['id']);
      $this->db->update('siswa', $object);

      return $this->db->affected_rows();
    }

    public function delete($id)
    {
      $this->db->where('id', $id);
      $this->db->delete('siswa');
      
      return $this->db->affected_rows();
    }

  }

 ?>