<?php 

defined('BASEPATH') OR exit ('No direct script access allowed');

  /**
   * 
   */
  class M_mapping extends CI_Model
  {
    
    public function getData()
    {
      $this->db->select('mapping_kelas.id as key,mapping_kelas.id_siswa,mapping_kelas.id_kelas,siswa.id,siswa.nama,siswa.umur,kelas.id,kelas.nama as nama_kelas');
      $this->db->from('mapping_kelas');
      $this->db->join('siswa','mapping_kelas.id_siswa = siswa.id','inner');
      $this->db->join('kelas','mapping_kelas.id_kelas = kelas.id','inner');
      $data = $this->db->get();

      return $data->result();
    }

    public function getListSiswa()
    {
      $data = $this->db->get('siswa');

      return $data->result();
    }

    public function getListKelas()
    {
      $data = $this->db->get('kelas');

      return $data->result();
    }

    public function insertMapping($param)
    {
      $object = [
        'id_siswa' => $param['id_siswa'],
        'id_kelas' => $param['id_kelas']
      ];
      $this->db->insert('mapping_kelas', $object);

      return $this->db->affected_rows();
    }

    public function editMapping($param){
      $object = [
        'id_siswa' => $param['id_siswa'],
        'id_kelas' => $param['id_kelas']
      ];
      $this->db->where('id', $param['id']);
      $this->db->update('mapping_kelas', $object);

      return $this->db->affected_rows();
    }

    public function getDetail($id)
    {
      $this->db->where('id', $id);
      $data = $this->db->get('mapping_kelas')->result();
      return $data[0];
    }

    function delete($id)
    {
        $this->db->where('id' ,$id);
        $this->db->delete('mapping_kelas');
        return  $this->db->affected_rows();
    }

// $this->db->select('tbl_user.username,tbl_user.userid,tbl_usercategory.type');
// $this->db->from('tbl_user');
// $this->db->join('tbl_usercategory','tbl_usercategory.usercategoryid=tbl_user.usercategoryid','inner');
// $query=$this->db->get();

//      $tampil = "SELECT m.id id, s.nama nama, k.nama kelas FROM siswa s, kelas k, mapping_kelas m WHERE m.id_siswa = s.id and m.id_kelas = k.id ORDER BY m.id";
//   $query  = $conn->query($tampil);

  }

 ?>