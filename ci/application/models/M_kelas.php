<?php 

defined('BASEPATH') OR exit ('No direct script access allowed');

  /**
   * 
   */
  class M_kelas extends CI_Model
  {
    
    public function getData()
    {
      $this->db->select('*');
      $this->db->from('kelas');
      $data = $this->db->get();

      return $data->result();
    }

    public function getDetailKelas($id)
    {
      $this->db->where('id',$id);
      $data = $this->db->get('kelas')->result();
      return $data[0];
    }

    public function insertKelas($param)
    {
      $object = [
        'nama' => $param['nama']
      ];
      $this->db->insert('kelas', $object);

      return $this->db->affected_rows();
    }

    public function editKelas($param)
    {
      $object = [
        'nama' => $param['nama']
      ];
      $this->db->where('id',$param['id']);
      $this->db->update('kelas',$object);

      return $this->db->affected_rows();
    }

    public function delete($id)
    {
      $this->db->where('id',$id);
      $this->db->delete('kelas');

      return $this->db->affected_rows();
    }
  }

 ?>