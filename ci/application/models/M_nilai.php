<?php 

defined('BASEPATH') OR exit ('No direct script access allowed');

  /**
   * 
   */
  class M_nilai extends CI_Model
  {
    
    public function getData()
    {
      $this->db->select('penilaian.id as key,penilaian.id_siswa,penilaian.id_mapel,penilaian.nilai,siswa.id,siswa.nama,siswa.umur,mapel.id,mapel.mapel');
      $this->db->from('penilaian');
      $this->db->join('siswa','siswa.id=penilaian.id_siswa','inner');
      $this->db->join('mapel','mapel.id=penilaian.id_mapel','inner');
      $data = $this->db->get();

      return $data->result();
    }

     public function getListSiswa()
    {
      $data = $this->db->get('siswa');

      return $data->result();
    }

    public function getListMapel()
    {
      $data = $this->db->get('mapel');

      return $data->result();
    }

    public function insertNilai($param)
    {
      $object = [
        'id_siswa' => $param['id_siswa'],
        'id_mapel' => $param['id_mapel'],
        'nilai'    => $param['nilai']
      ];
      $this->db->insert('penilaian', $object);

      return $this->db->affected_rows();
    }

    public function editNilai($param){
      $object = [
        'id_siswa' => $param['id_siswa'],
        'id_mapel' => $param['id_mapel'],
        'nilai'    => $param['nilai']
      ];
      $this->db->where('id', $param['id']);
      $this->db->update('penilaian', $object);

      return $this->db->affected_rows();
    }

    public function getDetail($id)
    {
      $this->db->where('id', $id);
      $data = $this->db->get('penilaian')->result();
      return $data[0];
    }

    function delete($id)
    {
        $this->db->where('id' ,$id);
        $this->db->delete('penilaian');
        return  $this->db->affected_rows();
    }

  }

 ?>