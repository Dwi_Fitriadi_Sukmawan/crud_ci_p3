<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Data Nilai</title>
  <link rel="stylesheet" href="">
</head>
<body>
  <table>
    <caption>Nilai </caption>
      <tr>
        <td>
          <a href="<?=site_url('home')?>">Home</a> ||
        </td>
        <td>
          <a href="<?=site_url('siswa')?>">Siswa</a> ||
        </td>
        <td>
          <a href="<?=site_url('mapel')?>">Mata Pelajaran</a> ||
        </td>
        <td>
          <a href="<?=site_url('kelas')?>">Kelas</a> ||
        </td>
        <td>
          <a href="<?=site_url('mapping')?>">Mapping Kelas</a> ||
        </td>
        <td>
          <a href="<?=site_url('nilai')?>">Nilai</a>
        </td>
      </tr>
  </table>
  <br>
  <hr />
  Data Nilai Siswa Sekolah P3 :
  <hr />
  <br>
  <a href="<?=site_url('nilai/form')?>">+ Tambah Data</a>
  <table border="1" cellspacing="0" cellpadding="10">
    <!-- <caption>Info Data Siswa</caption> -->
    <thead>
      <tr>
        <th>#</th>
        <th>No.</th>
        <th>Siswa</th>
        <th>Mapel</th>
        <th>Nilai</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody>
      <?php 
      $no = 1;
        foreach ($dataNilai as $key => $value) {
      ?>
      <tr>
        <td>
          <input type="checkbox" name="checkbox">
          <input type="hidden" name="id" value="<?=$value->key?>">
        </td>
        <td><?=$no++?></td>
        <td><?=$value->nama;?></td>
        <td><?=$value->mapel;?></td>
        <td><?=$value->nilai;?></td>
        <td>
          <a href="<?=site_url('nilai/form/'.$value->key)?>">Edit</a> ||
          <a href="<?=site_url('nilai/delete/'.$value->key)?>">Hapus</a>
        </td>
      </tr>
    <?php } ?>
    </tbody>
  </table>
</body>
</html>