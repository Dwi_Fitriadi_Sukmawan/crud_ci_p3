<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Data Siswa</title>
  <link rel="stylesheet" href="">
</head>
<body>
  <table>
    <caption>Siswa </caption>
      <tr>
        <td>
          <a href="<?=site_url('home')?>">Home</a> ||
        </td>
        <td>
          <a href="<?=site_url('siswa')?>">Siswa</a> ||
        </td>
        <td>
          <a href="<?=site_url('mapel')?>">Mata Pelajaran</a> ||
        </td>
        <td>
          <a href="<?=site_url('kelas')?>">Kelas</a> ||
        </td>
        <td>
          <a href="<?=site_url('mapping')?>">Mapping Kelas</a> ||
        </td>
        <td>
          <a href="<?=site_url('nilai')?>">Nilai</a>
        </td>
      </tr>
  </table>
  <br>
  <hr />
  Data Siswa Sekolah P3 :
  <hr />
  <br>
  <a href="<?=site_url('siswa/form')?>">+ Tambah Siswa</a>
  <table border="1" cellspacing="0" cellpadding="10">
    <!-- <caption>Info Data Siswa</caption> -->
    <thead>
      <tr>
        <th>No.</th>
        <th>Nama</th>
        <th>Umur</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody>
      <?php 
      $no = 1;
        foreach ($dataSiswa as $key => $value) {
      ?>
      <tr>
        <td><?=$no++?></td>
        <td><?=$value->nama;?></td>
        <td><?=$value->umur;?></td>
        <td>
          <a href="<?=site_url('siswa/form/'.$value->id)?>">Edit</a>
          <a href="<?=site_url('siswa/delete/'.$value->id)?>">Hapus</a>
        </td>
      </tr>
    <?php } ?>
    </tbody>
  </table>
</body>
</html>