<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Belajar Codeigniter</title>
  <link rel="stylesheet" href="">
</head>
<body>
  <h1>Hello, World!</h1>
  <p>
    Nama : <u><?=$nama?></u>
  </p>
  <p>
    Alamat : <u><?=$almt?></u>
  </p>
  <table>
    <caption>Home </caption>
      <tr>
        <td>
          <a href="<?=site_url('home')?>">Home</a> ||
        </td>
        <td>
          <a href="<?=site_url('siswa')?>">Siswa</a> ||
        </td>
        <td>
          <a href="<?=site_url('mapel')?>">Mata Pelajaran</a> ||
        </td>
        <td>
          <a href="<?=site_url('kelas')?>">Kelas</a> ||
        </td>
        <td>
          <a href="<?=site_url('mapping')?>">Mapping Kelas</a> ||
        </td>
        <td>
          <a href="<?=site_url('nilai')?>">Nilai</a>
        </td>
      </tr>
  </table>
</body>
</html>