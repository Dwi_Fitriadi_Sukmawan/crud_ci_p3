<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Form Siswa</title>
  <link rel="stylesheet" href="">
</head>
<body>
  <h1>Form Siswa</h1>
  <form action="<?=site_url('siswa/save')?>" method="POST" accept-charset="utf-8">
    <label>Nama : </label>
    <input type="text" name="nama" value="<?=@$detail->nama?>">
    <label>Umur : </label>
    <input type="number" name="umur" value="<?=@$detail->umur?>">
    <input type="hidden" name="id" value="<?=@$detail->id?>">
    <input type="hidden" name="act" value="<?=$act?>">
    <input type="submit" name="simpan" value="simpan">
  </form>
</body>
</html>