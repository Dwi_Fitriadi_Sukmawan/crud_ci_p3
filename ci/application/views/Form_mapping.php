<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Form Mapping</title>
  <link rel="stylesheet" href="">
</head>
<body>
  <h1>Form Mapping Kelas :</h1>
  <form action="<?=site_url('mapping/save')?>" method="POST">
    <label>Nama :</label>
    <select name="id_siswa">
      <?php foreach ($list_siswa as $key => $value): ?>
        <?php 
          $selected = "";
          if($value->id == @$detail->id_siswa){
            $selected = "selected";
          }
        ?>
        <option value="<?=$value->id?>" <?=$selected?>><?=$value->nama?></option>
      <?php endforeach ?>
    </select>
    <select name="id_kelas">
      <?php foreach ($list_kelas as $key => $value): ?>
        <?php 
          $selected = "";
          if($value->id == @$detail->id_kelas){
            $selected = "selected";
          }
        ?>
        <option value="<?=$value->id?>" <?=$selected?>><?=$value->nama?></option>
      <?php endforeach ?>
    </select>
    <input type="hidden" name="act" value="<?=$act?>">
    <input type="hidden" name="id" value="<?=@$detail->id?>">
    <input type="submit" name="simpan" value="Simpan">
  </form>
</body>
</html>