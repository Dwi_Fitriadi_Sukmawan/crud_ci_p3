<?php 

defined('BASEPATH') OR exit ('No direct script access allowed');

  /**
   * 
   */
  class Kelas extends CI_Controller
  {

    public function __construct()
    {
      parent::__construct();
        $this->load->model('M_kelas', 'kelas');
      // echo "Ini Function __construct Siswa <br>";
    }

    public function index()
    {
      $data['dataKelas'] = $this->kelas->getData();

      // echo "<pre>";
      // print_r($data);
      // echo "</pre>";

      $this->load->view('kelas', $data);
    }

    public function form()
    {
      $id = $this->uri->segment(3);

      if ($id != ''){
          $data['act']    = 'edit';
          $data['detail'] = $this->kelas->getDetailKelas($id);
      } else{
          $data['act'] = 'insert';
      }
      // print_r($data);
      $this->load->view('form_kelas', $data);
    }

    public function save()
    {
      $param = $this->input->post();

      if ($param['act'] == 'insert') {
          $this->kelas->insertKelas($param);
      } else if($param['act'] == 'edit') {
          $this->kelas->editKelas($param);
      }
      redirect('kelas');
    }

    public function delete($id)
    {
      $this->kelas->delete($id);
      redirect('kelas');
    }

    public function test()
    {
      echo "Ini Function Test Uri-Segment Siswa = ";
      echo $this->uri->segment(2);
    }

  }

 ?>