<?php 

defined('BASEPATH') OR exit ('No direct script access allowed');

  /**
   * 
   */
  class Mapping extends CI_Controller
  {

    public function __construct()
    {
      parent::__construct();
        $this->load->model('M_mapping', 'mapping');
    }

    public function index()
    {
      $data['dataMapping'] = $this->mapping->getData();
     
      // echo "<pre>";
      // print_r($data);
      // echo "</pre>";

      $this->load->view('mapping', $data);
    }

    public function form()
    {
      $id = $this->uri->Segment(3);

      if ($id != '') {
          $data['act']    = 'edit';
          $data['detail'] = $this->mapping->getDetail($id);
        } else {
          $data['act'] = 'insert';
        }

      $data['list_siswa'] = $this->mapping->getListSiswa();
      $data['list_kelas'] = $this->mapping->getListKelas();

      $this->load->view('form_mapping', $data);
      
    }

    public function save()
    {
      $param = $this->input->post();

      if($param['act'] == 'insert'){
        $this->mapping->insertMapping($param);
      }else if($param['act'] == 'edit'){
        $this->mapping->editMapping($param);
      }

      redirect('mapping');
    }

    public function delete()
    {
      $id = $this->uri->segment(3);

      if($id != ''){
        $this->mapping->delete($id);
      }
      redirect('mapping');
    }

    public function test()
    {
      echo "Ini Function Test Uri-Segment Siswa = ";
      echo $this->uri->segment(2);
    }

  }

 ?>