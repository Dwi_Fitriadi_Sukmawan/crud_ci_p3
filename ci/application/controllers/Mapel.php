<?php 

defined('BASEPATH') OR exit ('No direct script access allowed');

  /**
   * 
   */
  class Mapel extends CI_Controller
  {

    public function __construct()
    {
      parent::__construct();
        $this->load->model('M_mapel', 'mapel');
      // echo "Ini Function __construct Siswa <br>";
    }

    public function index()
    {
      $data['dataMapel'] = $this->mapel->getData();

      // echo "<pre>";
      // print_r($data);
      // echo "</pre>";


      $this->load->view('mapel', $data);
    }

    public function form()
    {
      $id = $this->uri->segment(3);

      if ($id != '') {
        $data['act']    = 'edit';
        $data['detail'] = $this->mapel->getDetailMapel($id);
      }else {
        $data['act'] = 'insert';
      }

      $this->load->view('Form_mapel',$data);
    }

    public function save()
    {
      $param = $this->input->post();

      if ($param['act'] == 'insert') {
        $this->mapel->insertMapel($param);
      } else if ($param['act'] == 'edit'){
        $this->mapel->editMapel($param);
      }
      
      redirect('mapel');
    }

    public function delete($id){
      $this->mapel->delete($id);

      redirect('mapel');
    }

    public function test()
    {
      echo "Ini Function Test Uri-Segment Siswa = ";
      echo $this->uri->segment(2);
    }

  }

 ?>