<?php 

defined('BASEPATH') OR exit ('No direct script access allowed');

  /**
   * 
   */
  class Nilai extends CI_Controller
  {

    public function __construct()
    {
      parent::__construct();
        $this->load->model('M_nilai', 'nilai');
      // echo "Ini Function __construct Siswa <br>";
    }

    public function index()
    {
      $data['dataNilai'] = $this->nilai->getData();

      // echo "<pre>";
      // print_r($data);
      // echo "</pre>";

      $this->load->view('nilai', $data);
    }

    public function form()
    {
      $id = $this->uri->Segment(3);

      if ($id != '') {
          $data['act']    = 'edit';
          $data['detail'] = $this->nilai->getDetail($id);
        } else {
          $data['act'] = 'insert';
        }

      $data['list_siswa'] = $this->nilai->getListSiswa();
      $data['list_mapel'] = $this->nilai->getListMapel();

      $this->load->view('Form_nilai', $data);
      
    }

    public function save()
    {
      $param = $this->input->post();

      if($param['act'] == 'insert'){
        $this->nilai->insertNilai($param);
      }else if($param['act'] == 'edit'){
        $this->nilai->editNilai($param);
      }

      redirect('nilai');
    }

    public function delete()
    {
      $id = $this->uri->segment(3);

      if($id != ''){
        $this->nilai->delete($id);
      }
      redirect('nilai');
    }

    public function test()
    {
      echo "Ini Function Test Uri-Segment Siswa = ";
      echo $this->uri->segment(2);
    }

  }

 ?>