<?php 

defined('BASEPATH') OR exit ('No direct script access allowed');

  /**
   * 
   */
  class Siswa extends CI_Controller
  {

    public function __construct()
    {
      parent::__construct();
        $this->load->model('M_siswa', 'myModel');
      // echo "Ini Function __construct Siswa <br>";
    }

    public function index()
    {
      $data['dataSiswa'] = $this->myModel->getData();

      // echo "<pre>";
      // print_r($data);
      // echo "</pre>";

      $this->load->view('siswa', $data);
    }

    public function form(){
      $id = $this->uri->segment(3);

      if ($id != ''){
          $data['act'] = 'edit';
          $data['detail'] = $this->myModel->getDetailSiswa($id);
      }else{
          $data['act'] = 'insert';
        }
      // print_r($data);
      $this->load->view('form_siswa', $data);
    }

    public function save(){
      $param = $this->input->post();

      if($param['act'] == 'insert'){
        $this->myModel->insertSiswa($param);
      }else if($param['act'] == 'edit'){
        $this->myModel->editSiswa($param);
      }
      // print_r($param);
      redirect('siswa');
    }

    public function delete($id){
      $this->myModel->delete($id);
      redirect('siswa');
    }

    public function test()
    {
      echo "Ini Function Test Uri-Segment Siswa = ";
      echo $this->uri->segment(2);
    }

  }

 ?>