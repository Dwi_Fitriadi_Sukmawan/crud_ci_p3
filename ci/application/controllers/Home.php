<?php 

  defined('BASEPATH') OR exit ('No direct script access allowed');

  /**
   * 
   */
  class Home extends CI_Controller
  {
    
    public function index()
    {
      $data['nama'] = 'Dwi Fitriadi Sukmawan';
      $data['almt'] = 'Malang, Batu Jawa Timur';

      $this->load->view('home', $data);
    }

    public function test()
    {
      echo "Ini Function Test Uri-Segment = ";
      echo $this->uri->segment(3);
    }
  }



?>